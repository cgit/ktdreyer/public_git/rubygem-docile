%global gem_name docile

Summary:       Docile keeps your Ruby DSLs tame and well-behaved
Name:          rubygem-%{gem_name}
Version:       1.1.3
Release:       5%{?dist}
Group:         Development/Languages
License:       MIT
URL:           https://ms-ati.github.io/docile/
Source0:       https://rubygems.org/gems/%{gem_name}-%{version}.gem
# Make simplecov/coveralls deps optional
# https://github.com/ms-ati/docile/pull/15
Patch0:        rubygem-docile-1.1.3-simplecov.patch
%if 0%{?fc19} || 0%{?fc20} || 0%{?el7}
Requires:      ruby(release)
Requires:      ruby(rubygems) 
%endif
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: rubygem(mime-types)
BuildRequires: rubygem(rake)
BuildRequires: rubygem(redcarpet)
BuildRequires: rubygem(rspec)
BuildRequires: rubygem(yard)
BuildArch:     noarch
%if 0%{?fc19} || 0%{?fc20} || 0%{?el7}
Provides:      rubygem(%{gem_name}) = %{version}
%endif

%description
Docile turns any Ruby object into a DSL.
Especially useful with the Builder pattern.


%package doc
Summary:   Documentation for %{name}
Group:     Documentation
Requires:  %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}

%prep
gem unpack %{SOURCE0}
%setup -q -D -T -n  %{gem_name}-%{version}
gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

# Make simplecov/coveralls dep optional
%patch0 -p1

%build
gem build %{gem_name}.gemspec
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a ./%{gem_dir}/* %{buildroot}%{gem_dir}/

# Remove build leftovers.
rm -rf %{buildroot}%{gem_instdir}/{.coveralls.yml,.gitignore,.rspec,.ruby-gemset,.ruby-version,.travis.yml,.yard*}

%check
%if 0%{?with_tests}
rspec -Ilib spec
%endif

%files
%doc %{gem_instdir}/LICENSE
%dir %{gem_instdir}
%{gem_libdir}
%exclude %{gem_cache}
%exclude %{gem_instdir}/Gemfile
%exclude %{gem_instdir}/docile.gemspec
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/HISTORY.md
%doc %{gem_instdir}/README.md
%{gem_instdir}/Rakefile
%{gem_instdir}/spec

%changelog
* Wed Jun 11 2014 Ken Dreyer <ktdreyer@ktdreyer.com> - 1.1.3-5
- Use HTTPS URL

* Wed Jun 11 2014 Ken Dreyer <ktdreyer@ktdreyer.com> - 1.1.3-4
- Adjustments for https://fedoraproject.org/wiki/Changes/Ruby_2.1

* Wed Jun 11 2014 Ken Dreyer <ktdreyer@ktdreyer.com> - 1.1.3-3
- Unconditionally run tests

* Wed Jun 11 2014 Ken Dreyer <ktdreyer@ktdreyer.com> - 1.1.3-2
- Patch to make simplecov/coveralls optional

* Wed Apr 02 2014 Troy Dawson <tdawson@redhat.com> - 1.1.3-1
- Initial package
